<?php
//a)
$a="Bangladesh";
echo gettype($a);
echo "<br>";

//b)
if (isset($a))
{
    echo "I love Bangladesh";
}
//c)

echo "<br>";
if (is_object($a))
{
    echo " This is a object";
}
else
{
    echo "Opps! This is not a object";
}

//d)
echo "<br>";
var_dump($a);

//e)
echo "<br>";
if (is_array($a))
{
    echo "Yes this is array";
}
else
{
    echo "Not array";}