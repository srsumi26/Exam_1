<?php
//floatval
$a=12.2;
echo floatval($a);

//is_array
echo "<br>";
$b=array(12,14);

if (is_array($b))
{
    echo "true";
}

//is_null
echo "<br>";
if (is_null($a))
{
    echo "true";
}
else
{
    echo "false";
}

//is_object
echo "<br>";
if (is_object($a))
{
    echo "true";
}
else
{
    echo "false";
}
//print_r
echo "<br>";

print_r($b);

//serilize
echo "<br>";

$e=array("I","am","a","student");
print_r(serialize($e));

//is bool
echo "<br>";
$n=true;
echo is_bool($n);

//is float
echo "<br>";
$m=12.5;
echo is_float($m);